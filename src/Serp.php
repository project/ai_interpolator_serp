<?php

namespace Drupal\ai_interpolator_serp;

use Drupal\ai_interpolator\Exceptions\AiInterpolatorRequestErrorException;
use Drupal\ai_interpolator_serp\Form\SerpConfigForm;
use Drupal\Core\Config\ConfigFactory;
use GuzzleHttp\Client;

/**
 * SERP API.
 */
class Serp {

  /**
   * The http client.
   */
  protected Client $client;

  /**
   * API Key.
   */
  private string $apiKey;

  /**
   * The base path.
   */
  private string $basePath = 'https://serpapi.com/';

  /**
   * Constructs a new SERP object.
   *
   * @param \GuzzleHttp\Client $client
   *   Http client.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   */
  public function __construct(Client $client, ConfigFactory $configFactory) {
    $this->client = $client;
    $config = $configFactory->get(SerpConfigForm::CONFIG_NAME);
    $this->apiKey = $config->get('api_key') ?? '';
  }

  /**
   * Search API Call.
   *
   * @param array $parameters
   *   The parameters for the call. q have to be set.
   *
   * @return array
   *   The search results.
   */
  public function searchCall(array $parameters) {
    if (empty($parameters['q'])) {
      throw new AiInterpolatorRequestErrorException('You have to set a search query');
    }
    $parameters['engine'] = 'google';
    return json_decode($this->makeRequest('search.json', $parameters), TRUE);
  }

  /**
   * Make SERP call.
   *
   * @param string $path
   *   The path.
   * @param array $query_string
   *   The query string.
   * @param string $method
   *   The method.
   * @param string $json
   *   JSON params.
   *
   * @return string|object
   *   The return response.
   */
  public function makeRequest($path, array $query_string = [], $method = 'GET', $json = '') {
    // Headers.
    $query_string['api_key'] = $this->apiKey;

    // We can wait some.
    $options['connect_timeout'] = 30;
    $options['read_timeout'] = 30;
    // Don't let Guzzle die, just forward body and status.
    $options['http_errors'] = FALSE;

    $options['headers']['Content-Type'] = 'application/json';


    $apiEndPoint = $this->basePath . $path;
    $apiEndPoint .= count($query_string) ? '?' . http_build_query($query_string) : '';

    if ($json) {
      $options['body'] = json_encode($json);
    }

    $res = $this->client->request($method, $apiEndPoint, $options);
    return $res->getBody();
  }

}
