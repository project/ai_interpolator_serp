<?php

namespace Drupal\ai_interpolator_serp\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\Annotation\AiInterpolatorFieldRule;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_serp\Serp;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The rules for an SERP link field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_serp_to_link",
 *   title = @Translation("SERP To Link"),
 *   field_rule = "link"
 * )
 */
class SerpSearchToLink extends AiInterpolatorFieldRule implements AiInterpolatorFieldRuleInterface, ContainerFactoryPluginInterface {

  /**
   * Google Domains.
   */
  public $domains = [
    'google.com',
    'google'
  ];

  /**
   * {@inheritDoc}
   */
  public $title = 'SERP to Link';

  /**
   * The SERP api service.
   */
  public Serp $serp;

  /**
   * The Path resolver.
   */
  public ExtensionPathResolver $pathResolver;

  /**
   * Construct an image field.
   *
   * @param array $configuration
   *   Inherited configuration.
   * @param string $plugin_id
   *   Inherited plugin id.
   * @param mixed $plugin_definition
   *   Inherited plugin definition.
   * @param \Drupal\ai_interpolator_serp\Serp $serp
   *   The SERP service.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $pathResolver
   *   The Path resolver service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    Serp $serp,
    ExtensionPathResolver $pathResolver,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->serp = $serp;
    $this->pathResolver = $pathResolver;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('ai_interpolator_serp.api'),
      $container->get('extension.path.resolver')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function needsPrompt() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function advancedMode() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function allowedInputs() {
    return [
      'text',
      'string',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function helpText() {
    return $this->t("Finds links based on a search string Google search.");
  }

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "";
  }

  /**
   * {@inheritDoc}
   */
  public function extraFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    $cardinality = $fieldDefinition->getFieldStorageDefinition()->getCardinality();
    $defaultGeneration = $cardinality < 0 || $cardinality > 10 ? 10 : $cardinality;
    $textAmount = $cardinality == -1 ? 'unlimited' : $cardinality;

    $form = [];
    $this->addTokenConfigurationToggle($form, $entity, $fieldDefinition);

    $form['interpolator_serp_search_amount'] = [
      '#type' => 'select',
      '#options' => array_combine(range(1, 40), range(1, 40)),
      '#title' => 'Search Results',
      '#description' => $this->t('Amount of results to fetch (set %amount).', [
        '%amount' => $textAmount,
      ]),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_serp_search_amount', $defaultGeneration),
    ];

    $this->addTokenConfigurationFormField('interpolator_serp_search_amount', $form, $entity, $fieldDefinition);

    $form['interpolator_serp_search_offset'] = [
      '#type' => 'number',
      '#title' => 'Search Offset',
      '#description' => $this->t('Offset to start from if you are doing multiple calls.', [
        '%amount' => $textAmount,
      ]),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_serp_search_offset', 0),
    ];

    $this->addTokenConfigurationFormField('interpolator_serp_search_offset', $form, $entity, $fieldDefinition);

    $form['interpolator_serp_search_type'] = [
      '#type' => 'select',
      '#options' => [
        '' => $this->t('Google Search'),
        'nws' => $this->t('News Search'),
      ],
      '#title' => 'Search Type',
      '#description' => $this->t('The search type to use.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_serp_search_type', ''),
    ];

    $this->addTokenConfigurationFormField('interpolator_serp_search_type', $form, $entity, $fieldDefinition);

    $form['interpolator_serp_search_google_domain'] = [
      '#type' => 'select',
      '#options' => $this->getDomains(),
      '#title' => 'Google Domain',
      '#description' => $this->t('The Google Domain to use.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_serp_search_google_domain', 'google.com'),
    ];

    $this->addTokenConfigurationFormField('interpolator_serp_search_google_domain', $form, $entity, $fieldDefinition);

    $form['interpolator_serp_search_language'] = [
      '#type' => 'select',
      '#options' => $this->getLanguages(),
      '#title' => 'Google Language',
      '#description' => $this->t('The language to use.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_serp_search_language', 'en'),
    ];

    $this->addTokenConfigurationFormField('interpolator_serp_search_language', $form, $entity, $fieldDefinition);

    $form['interpolator_serp_search_country'] = [
      '#type' => 'select',
      '#options' => $this->getCountries(),
      '#title' => 'Google Country',
      '#description' => $this->t('The country to use.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_serp_search_country', 'us'),
    ];

    $this->addTokenConfigurationFormField('interpolator_serp_search_country', $form, $entity, $fieldDefinition);

    $form['interpolator_serp_search_force_language_detail'] = [
      '#type' => 'details',
      '#title' => $this->t('Force languages'),
    ];

    $form['interpolator_serp_search_force_language_detail']['interpolator_serp_search_force_language'] = [
      '#type' => 'checkboxes',
      '#multiple' => TRUE,
      '#options' => $this->getForcedLanguages(),
      '#title' => 'Google Force Languages',
      '#description' => $this->t('Only give back results in these languages.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_serp_search_force_language', []),
    ];

    $this->addTokenConfigurationFormField('interpolator_serp_search_force_language', $form, $entity, $fieldDefinition, 'interpolator_serp_search_force_language_detail');

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function generate(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig) {
    // Prepare call.
    $parameters['q'] = $entity->{$interpolatorConfig['base_field']}->value;
    if (!empty($interpolatorConfig['serp_search_type'])) {
      if ($interpolatorConfig['serp_search_type'] == 'nws') {
        $parameters['engine'] = 'google_news';
      }
    }
    if ($searchAmount = $this->getConfigValue('serp_search_amount', $interpolatorConfig, $entity)) {
      $parameters['num'] = $searchAmount;
    }
    if (!empty($interpolatorConfig['serp_search_google_domain'])) {
      $parameters['google_domain'] = $interpolatorConfig['serp_search_google_domain'];
    }
    if (!empty($interpolatorConfig['serp_search_language'])) {
      $parameters['hl'] = $interpolatorConfig['serp_search_language'];
    }
    if (!empty($interpolatorConfig['serp_search_country'])) {
      $parameters['gl'] = $interpolatorConfig['serp_search_country'];
    }
    if (!empty($interpolatorConfig['serp_search_force_language'])) {
      $force = [];
      foreach ($interpolatorConfig['serp_search_force_language'] as $val) {
        if ($val) {
          $force[] = $val;
        }
      }
      if (count($force)) {
        $parameters['lr'] = implode('|', $force);
      }
    }
    $result = $this->serp->searchCall($parameters);
    if (empty($result['organic_results'][0]['link'])) {
      return [];
    }
    $links = [];
    foreach ($result['organic_results'] as $link) {
      $links[] = [
        'uri' => $link['link'],
        'title' => $link['title'],
      ];
    }
    return $links;
  }

  /**
   * {@inheritDoc}
   */
  public function verifyValue(ContentEntityInterface $entity, $value, FieldDefinitionInterface $fieldDefinition) {
    if (is_array($value) && !empty($value['uri'])) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Helper function to get domains.
   *
   * @return array
   *   An options array.
   */
  public function getDomains() {
    $domains = json_decode(file_get_contents($this->pathResolver->getPath('module', 'ai_interpolator_serp') . '/resources/google-domains.json'), TRUE);
    $return = [
      '' => $this->t('-- none --'),
    ];
    foreach ($domains as $domain) {
      $return[$domain['domain']] = "$domain[country_name] ($domain[domain])";
    }
    return $return;
  }

  /**
   * Helper function to get languages.
   *
   * @return array
   *   An options array.
   */
  public function getLanguages() {
    $languages = json_decode(file_get_contents($this->pathResolver->getPath('module', 'ai_interpolator_serp') . '/resources/google-languages.json'), TRUE);
    $return = [
      '' => $this->t('-- none --'),
    ];
    foreach ($languages as $language) {
      $return[$language['language_code']] = "$language[language_name]";
    }
    return $return;
  }

  /**
   * Helper function to get forced languages.
   *
   * @return array
   *   An options array.
   */
  public function getForcedLanguages() {
    $languages = json_decode(file_get_contents($this->pathResolver->getPath('module', 'ai_interpolator_serp') . '/resources/google-lr-languages.json'), TRUE);
    $return = [];
    foreach ($languages as $language) {
      $return[$language['language_code']] = "$language[language_name]";
    }
    return $return;
  }

  /**
   * Helper function to get countries.
   *
   * @return array
   *   An options array.
   */
  public function getCountries() {
    $countries = json_decode(file_get_contents($this->pathResolver->getPath('module', 'ai_interpolator_serp') . '/resources/google-countries.json'), TRUE);
    $return = [
      '' => $this->t('-- none --'),
    ];
    foreach ($countries as $country) {
      $return[$country['country_code']] = "$country[country_name]";
    }
    return $return;
  }

}
